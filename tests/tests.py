from cookiecutter.main import cookiecutter


param = \
{
    "full_name": "Yann de The",
    "email": "ydethe@gmail.com",
    "github_username": "ydethe",
    "project_name": "MyAwesomeProject",
    "project_short_description": "Python Boilerplate contains all the boilerplate you need to create a Python package.",
    "version": "0.1.0",
    "add_pyup_badge": "y",
    "command_line_interface": "y",
    "open_source_license": "MIT license",
  }

cookiecutter('.', no_input=True, extra_context=param, overwrite_if_exists=True)

