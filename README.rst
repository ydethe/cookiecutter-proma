==================
cookiecutter-proma
==================

.. image:: https://gitlab.com/ydethe/cookiecutter-proma/badges/master/pipeline.svg
   :target: https://gitlab.com/ydethe/cookiecutter-proma/pipelines
   

A cookiecutter for the proma project manager

To use it, just type::
    cookiecutter https://gitlab.com/ydethe/cookiecutter-proma.git


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

