{{cookiecutter.project_name}}, {{cookiecutter.year}}
{% for i in cookiecutter.project_name %}={% endfor %}{% for i in cookiecutter.year %}={% endfor %}==


.. image:: https://img.shields.io/pypi/v/{{cookiecutter.project_name}}.svg
        :target: https://pypi.python.org/pypi/{{cookiecutter.project_name}}

.. image:: https://readthedocs.org/projects/{{cookiecutter.project_slug}}/badge/?version=latest
        :target: https://{{cookiecutter.project_slug}}.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://gitlab.com/{{cookiecutter.github_username}}/{{cookiecutter.project_slug}}/badges/master/pipeline.svg
   :target: https://gitlab.com/{{cookiecutter.github_username}}/{{cookiecutter.project_slug}}/pipelines
   

Free software: MIT license

Documentation: https://{{cookiecutter.project_slug}}.readthedocs.io.

{{cookiecutter.project_short_description}}

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

