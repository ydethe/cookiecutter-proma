.. {{ cookiecutter.project_name }} documentation master file, created by
   sphinx-quickstart on Mon Jan  6 15:10:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{ cookiecutter.project_name }}'s documentation!
{% for i in cookiecutter.project_name %}={% endfor %}============================

.. include:: ../README.rst

Content
=======

.. toctree::
    :maxdepth: 1

    install
    tutorial
    modules
    
Indices and tables
==================

* :ref:`genindex`
