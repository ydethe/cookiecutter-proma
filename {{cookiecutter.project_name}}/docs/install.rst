.. Licensed under the MIT Licensed

.. _install:

============
Installation
============

.. highlight:: console

.. _SystemControl_pypi: https://pypi.org/project/{{ cookiecutter.project_name }}/
.. _setuptools: https://pypi.org/project/setuptools/


You can install {{ cookiecutter.project_name }} in the usual ways. The simplest way is with pip::

    $ pip install {{ cookiecutter.project_name }}

Checking the installation
-------------------------

If all went well, you should be able to open a command prompt, and see
{{ cookiecutter.project_name }} installed properly:

.. parsed-literal::

    $ python -m {{ cookiecutter.project_name }} --version
    {{ cookiecutter.project_name }}, version |release|
    Documentation at |doc-url|
