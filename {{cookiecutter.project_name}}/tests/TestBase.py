import os
import unittest
from typing import Iterable
import pathlib
from inspect import currentframe, getframeinfo

import numpy as np
import scipy.linalg as lin
import matplotlib.pyplot as plt


class TestBase(unittest.TestCase):
    def show(self, fig, _f_back=False):
        fig_title = fig.suptitle

        if _f_back:
            cf = currentframe().f_back.f_back
        else:
            cf = currentframe().f_back

        frameinfo = getframeinfo(cf)
        fic = frameinfo.filename
        fct = frameinfo.function

        if "SHOW_PLOT" in os.environ:
            plt.show()
        else:
            root, ext = os.path.splitext(os.path.basename(fic))
            dir = os.path.join("test-results", "graphics", root, fct)
            pathlib.Path(dir).mkdir(parents=True, exist_ok=True)
            fig.savefig(os.path.join(dir, fig_title + ".png"), papertype="a4")
            plt.close("all")
