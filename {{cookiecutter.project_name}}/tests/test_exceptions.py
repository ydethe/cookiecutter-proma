import os
import unittest

from {{cookiecutter.project_name}}.exceptions import *

try:
    from tests.TestBase import TestBase
except Exception as e:
    from TestBase import TestBase


def failing_computation(arg):
    raise SampleException

class TestExceptions(TestBase):
    def test_sample(self):
        self.assertRaises(SampleException, failing_computation, "foo")


if __name__ == "__main__":
    unittest.main()
